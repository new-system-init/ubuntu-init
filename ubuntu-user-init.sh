#!/bin/bash

#Confirm installation type
read -p 'Is this installation for Virtualbox? ([Y]/n)' vbox
vbox=${vbox:-y}
vbox=$(echo $vbox | awk '{print tolower($0)}')

#Setup user
uservar="$(who | awk '{print $1}')"
sudo usermod -aG sudo $uservar

#create scripts directory
mkdir /home/$uservar/scripts

#Get linenum
mkdir /home/$uservar/scripts/linenum
git clone https://github.com/rebootuser/LinEnum.git /home/$uservar/scripts/linenum

#Remove any locks on upgrade
sudo rm /var/cache/apt/archives/lock
sudo rm /var/lib/dpkg/lock
sudo rm /var/lib/dpkg/lock-frontend
sudo rm /var/lib/apt/lists/lock

#Update software
echo -e "\n****Updating System****\n"
sudo apt update
sudo apt upgrade -y

#Install wireshark silently
echo -e "\n****Installing Wireshark****\n"
sudo apt install -y debconf-utils
echo "wireshark-common wireshark-common/install-setuid boolean true" | sudo debconf-set-selections
sudo apt install -y wireshark
sudo usermod -aG wireshark $uservar

echo -e "\n****Installing Packages****\n"
sudo apt install -y htop vim nmap tshark nmap curl wget proxychains terminator \
chromium-browser build-essential dkms linux-headers-$(uname -r) \
gnome-session-flashback python-pip python3-dev python3-pip python3-setuptools vim gnome-tweak-tool \
sqlmap nikto libcurl4-openssl-dev libxml2 libxml2-dev libxslt1-dev ruby-dev libgmp-dev \
zlib1g-dev socat hashcat john aircrack-ng hping3 arc-theme papirus-icon-theme

#Install theFuck
sudo pip3 install thefuck
echo 'eval "$(thefuck --alias)"' >> /home/$uservar/.bashrc

#Install VSCode
echo -e "\n****Installing VSCode****\n"
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -o root -g root -m 644 packages.microsoft.gpg /usr/share/keyrings/
sudo sh -c 'echo "deb [arch=amd64 signed-by=/usr/share/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
sudo apt update
sudo apt install -y apt-transport-https
sudo apt install -y code
rm packages.microsoft.gpg

#Install ZAP
echo -e "\n****Installing ZAP****\n"
sudo sh -c "echo 'deb http://download.opensuse.org/repositories/home:/cabelo/xUbuntu_18.04/ /' > /etc/apt/sources.list.d/home:cabelo.list"
wget -nv https://download.opensuse.org/repositories/home:cabelo/xUbuntu_18.04/Release.key -O Release.key
sudo apt-key add - < Release.key
rm Release.key
sudo apt update
sudo apt install -y owasp-zap

echo -e "\n****Installing Burp Suite****\n"
wget "https://portswigger.net/burp/releases/download?product=community&version=2.1.04&type=linux&componentid=100" -O /home/$uservar/Downloads/burpsuite_community_linux_v2_1_04.sh
sudo bash /home/$uservar/Downloads/burpsuite_community_linux_v2_1_04.sh

#If Virtualbox installation
if [ $vbox == 'y' ];
then
    while true; do
        read -p 'Insert Guest Additions CD... (Press Enter when done.)'
        if [ -d "/media/$uservar/" ]; then
            break
        fi
    done
    guestversion=$(ls /media/$uservar)
    sudo sh /media/$uservar/$guestversion/VBoxLinuxAdditions.run --nox11
fi

#Change Wallpaper
echo -e "\n****Change Wallpaper****\n"
mkdir /home/$uservar/Pictures/Wallpapers
wget https://gitlab.com/new-system-init/ubuntu-init/raw/master/backgrounds/vader_smoke.jpg -O /home/$uservar/Pictures/Wallpapers/vader_smoke.jpg
gsettings set org.gnome.desktop.background picture-uri file:///home/$uservar/Pictures/Wallpapers/vader_smoke.jpg

#Setup Terminator Theme
pip install requests
mkdir -p $HOME/.config/terminator/plugins
wget https://git.io/v5Zww -O $HOME"/.config/terminator/plugins/terminator-themes.py"

#Remove Amazon Icon Crap
sudo rm /usr/share/applications/com.canonical.launcher.amazon.desktop
sudo rm /usr/share/applications/ubuntu-amazon-default.desktop

#Reboot
read -p 'Press Enter to reboot.'
shutdown -r now
